package org.garipov.cng.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;


@Service
public class CarNumberServiceImpl implements CarNumberService {


    @Qualifier("numbers")
    @Autowired
    private List<String> numberStorage;

    @Autowired
    private ExecutorService executorService;

    @Override
    public String generateCarNumber() {
//        Future<String> future = executorService.submit(() -> {
            final String REGION = "116 RUS";
            int index = new Random().nextInt(numberStorage.size());
            System.out.println(Thread.currentThread().getName());
            return numberStorage.get(index) + " " + REGION;
//        });
//        String result = null;
//        try {
//            result = future.get();
//        } catch (InterruptedException | ExecutionException e) {
//            e.printStackTrace();
//        }
//        return result;
    }
}
