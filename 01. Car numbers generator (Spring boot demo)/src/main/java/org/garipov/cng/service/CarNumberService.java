package org.garipov.cng.service;

public interface CarNumberService {

    String generateCarNumber();

}
