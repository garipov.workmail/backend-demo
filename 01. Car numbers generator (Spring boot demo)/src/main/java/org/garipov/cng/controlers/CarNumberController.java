package org.garipov.cng.controlers;

import org.garipov.cng.service.CarNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
public class CarNumberController {

    @Autowired
    CarNumberService carNumberService;

    @GetMapping("/number")
    public String start(){
        return "numbers";
    }

    @GetMapping("/number/random")
    public ResponseEntity<String> random() {
        return ResponseEntity.ok(carNumberService.generateCarNumber());
    }


    @GetMapping("/number/next")
    public String getNextCarNumber(Model model, @PathVariable String randomOrNextToggle){
        model.addAttribute("number", carNumberService.generateCarNumber());
        return "numbers";
    }
}
