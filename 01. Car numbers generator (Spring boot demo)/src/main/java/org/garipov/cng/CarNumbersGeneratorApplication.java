package org.garipov.cng;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@SpringBootApplication
public class CarNumbersGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarNumbersGeneratorApplication.class, args);
        System.out.println(Thread.currentThread().getName());
    }

    @Bean
    public ExecutorService executorService() {
        return Executors.newCachedThreadPool();
    }

    @Bean
    @Qualifier("numbers")
    List<String> numbersStorage() {
        String[] alphabet = new String[]{"А", "В", "Е", "К", "М", "Н", "О", "Р", "С", "Т", "У", "Х"};

        List<String> numbersList = new LinkedList<>();
        Arrays.stream(alphabet).forEach(
                firstLetter -> Arrays.stream(alphabet).forEach(
                        secondLetter -> Arrays.stream(alphabet).forEach(
                                thirdLetter -> IntStream.range(1, 1000).forEach(
                                        nums -> numbersList.add(String.format("%s%03d%s%s",
                                                firstLetter,
                                                nums,
                                                secondLetter,
                                                thirdLetter)))
                        )
                )
        );
        System.out.println(">>>Numbers storage started");
        return numbersList;
    }

}
