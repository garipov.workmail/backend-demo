package ru.garipov.mimi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.garipov.mimi.models.Cat;

@Repository
public interface CatsRepository extends JpaRepository<Cat, Long> {
}
