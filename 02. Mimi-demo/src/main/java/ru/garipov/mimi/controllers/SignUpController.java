package ru.garipov.mimi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.garipov.mimi.forms.UserForm;
import ru.garipov.mimi.services.SignUpService;

import java.net.Authenticator;

@Controller
public class SignUpController {

    @Autowired
    SignUpService signUpService;

    @PostMapping("/signUp")
    public String signUpUser(Model model, UserForm userForm){
        signUpService.signUp(userForm);
        return "redirect:/signIn";
    }

    @GetMapping("/signUp")
    public String getSignUpPage(Authenticator authenticator, Model model){
        if (authenticator == null) {
            model.addAttribute("userForm", new UserForm());
            return "sign_up_page";
        } else {
            return "redirect:/";
        }
    }
}
