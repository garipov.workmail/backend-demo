package ru.garipov.mimi.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Data
@EqualsAndHashCode
@Entity
public class Cat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String photo;

    private Integer rating;

    @ManyToMany(mappedBy = "cats")
    private List<User> users;

}
