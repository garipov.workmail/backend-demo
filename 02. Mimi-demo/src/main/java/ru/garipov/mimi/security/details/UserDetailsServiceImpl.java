package ru.garipov.mimi.security.details;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.garipov.mimi.models.User;
import ru.garipov.mimi.repositories.UsersRepository;


/**
 * 01.07.2020
 * 43. Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service(value = "customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = usersRepository.findByName(name).orElseGet(() -> {
            throw new UsernameNotFoundException("User not found");
        });
        return new UsersDetailsImpl(user);
    }
}
