package ru.garipov.mimi.forms;

import lombok.Data;

@Data
public class UserForm {
    private  Long id;
    private String name;
    private String password;
}
